package Teori.OperasiFile;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Tampilan extends JFrame implements ActionListener {
    private DefaultTableModel model;
    private JTable table;
    private JLabel NamaLengkap, Tanggallahir, NoPepndaftaran, Notelp, Alamat, Email;
    private JTextField txtnama, txttanggallahir, txtNoPepndaftaran, txtnotelp, txtalamat, txtemail;
    private JButton Submit, Batal, Hapus;

    private static final String HOST = "localhost";
    private static final String PORT = "3306";
    private static final String DATABASE_NAME = "pendaftaranmahasiswa";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "098765@12345";
    private static final String URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DATABASE_NAME;

    public Tampilan() {
        setTitle("Pendaftar Mahasiswa Baru");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 500);
        setLayout(new BorderLayout());

        JPanel inputPanel = new JPanel(new GridLayout(6, 2, 5, 5));
        inputPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        NamaLengkap = new JLabel("Nama: ");
        txtnama = new JTextField();
        Tanggallahir = new JLabel("Tanggal Lahir: ");
        txttanggallahir = new JTextField();
        NoPepndaftaran = new JLabel("No. Pendaftaran: ");
        txtNoPepndaftaran = new JTextField();
        Notelp = new JLabel("No. Telp: ");
        txtnotelp = new JTextField();
        Alamat = new JLabel("Alamat: ");
        txtalamat = new JTextField();
        Email = new JLabel("Email: ");
        txtemail = new JTextField();

        inputPanel.add(NamaLengkap);
        inputPanel.add(txtnama);
        inputPanel.add(Tanggallahir);
        inputPanel.add(txttanggallahir);
        inputPanel.add(NoPepndaftaran);
        inputPanel.add(txtNoPepndaftaran);
        inputPanel.add(Notelp);
        inputPanel.add(txtnotelp);
        inputPanel.add(Alamat);
        inputPanel.add(txtalamat);
        inputPanel.add(Email);
        inputPanel.add(txtemail);

        add(inputPanel, BorderLayout.NORTH);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        Batal = new JButton("Batal");
        Hapus = new JButton("Hapus");
        Submit = new JButton("Submit");

        Dimension buttonSize = new Dimension(100, 30);
        Batal.setPreferredSize(buttonSize);
        Hapus.setPreferredSize(buttonSize);
        Submit.setPreferredSize(buttonSize);

        buttonPanel.add(Submit);
        buttonPanel.add(Hapus);
        buttonPanel.add(Batal);

        add(buttonPanel, BorderLayout.SOUTH);

        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        model = new DefaultTableModel(new Object[]{"Nama", "Tanggal Lahir", "No. Pendaftaran", "No. Telp", "Alamat", "Email"}, 0);
        table = new JTable(model);
        tablePanel.add(new JScrollPane(table), BorderLayout.CENTER);

        add(tablePanel, BorderLayout.CENTER);

        Submit.addActionListener(this);

        Hapus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow != -1) {
                    model.removeRow(selectedRow);
                } else {
                    JOptionPane.showMessageDialog(null, "Pilih baris yang akan dihapus.");
                }
            }
        });

        Batal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtnama.setText("");
                txttanggallahir.setText("");
                txtNoPepndaftaran.setText("");
                txtnotelp.setText("");
                txtalamat.setText("");
                txtemail.setText("");
            }
        });

        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Submit) {
            String nama = txtnama.getText().trim();
            String tanggalLahir = txttanggallahir.getText().trim();
            String noPendaftaran = txtNoPepndaftaran.getText().trim();
            String noTelp = txtnotelp.getText().trim();
            String alamat = txtalamat.getText().trim();
            String email = txtemail.getText().trim();

            if (nama.isEmpty() || tanggalLahir.isEmpty() || noPendaftaran.isEmpty() || noTelp.isEmpty() || alamat.isEmpty() || email.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Semua kolom harus diisi.", "Kesalahan", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Object[] data = {nama, tanggalLahir, noPendaftaran, noTelp, alamat, email};
            model.addRow(data);

            txtnama.setText("");
            txttanggallahir.setText("");
            txtNoPepndaftaran.setText("");
            txtnotelp.setText("");
            txtalamat.setText("");
            txtemail.setText("");

            simpanKeDatabase(nama, tanggalLahir, noPendaftaran, noTelp, alamat, email);
        }
    }

    private void simpanKeDatabase(String nama, String tanggalLahir, String noPendaftaran, String noTelp, String alamat, String email) {
        try {
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            String sql = "INSERT INTO DataPendaftaran (Nama, Tanggal_Lahir, No_Pendaftaran, No_Telp, Alamat, Email) VALUES ('" + nama + "', '" + tanggalLahir + "', '" + noPendaftaran + "', '" + noTelp + "', '" + alamat + "', '" + email + "')";

            Statement statement = connection.createStatement();
            int rowsInserted = statement.executeUpdate(sql);
            if (rowsInserted > 0) {
                System.out.println("Data berhasil disimpan ke database.");
            }

            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Tampilan();
    }
}
